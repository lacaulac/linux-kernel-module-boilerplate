#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Me <nyancat@420.moe>");
MODULE_DESCRIPTION("My AWWW-SOME kernel module.");

static int __init init(void)
{
	printk(KERN_INFO "My awesome module just launched! Hurray!");
	return 0;
}

static void __exit exit(void)
{
	printk(KERN_INFO "My awesome module is being closed down. So saaaaad...");
}

module_init(init);
module_exit(exit);
